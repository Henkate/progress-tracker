package pl.kuben.progressapp.data.dao

import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.Query
import pl.kuben.progressapp.data.model.db.WebEntity

@Dao
interface WebEntityDao {

    @Insert
    fun addWebEntity(webEntity: WebEntity): Long

    @Insert
    fun addWebEntities(webEntities: List<WebEntity>): List<Long>

    @Query("DELETE FROM WebEntity WHERE progress_id = :progressId")
    fun deleteProgressWebEntities(progressId: Int): Int
}