package pl.kuben.progressapp.data.dao

import android.arch.persistence.room.*
import io.reactivex.Single
import pl.kuben.progressapp.data.model.db.Progress

@Dao
interface ProgressDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(progress: Progress): Long

    @Query("SELECT * FROM progress")
    fun getAll(): Single<List<Progress>>

    @Query("SELECT * FROM progress WHERE id = :progressId")
    fun getProgress(progressId: Int): Single<Progress>

    @Query("DELETE FROM progress WHERE id = :progressId")
    fun deleteProgress(progressId: Int): Int
}