package pl.kuben.progressapp.data.model.db

import android.arch.persistence.room.ColumnInfo
import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.Relation

@Entity
data class WebEntity(
        @ColumnInfo(name = "progress_id") val progressId: Long,
        @ColumnInfo(name = "web_progress_id") val webProgressId: Long
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long? = null

}