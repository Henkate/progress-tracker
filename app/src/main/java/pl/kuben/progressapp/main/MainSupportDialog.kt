package pl.kuben.progressapp.main

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.view.ViewGroup
import kotlinx.android.synthetic.main.dialog_main_support.*
import pl.kuben.progressapp.R
import pl.kuben.progressapp.common.openUrlIntent

class MainSupportDialog(context: Context) : AlertDialog(context) {

    var onAboutClick: (dialog: DialogInterface) -> Unit = {}
    var onDontShowClick: (dialog: DialogInterface) -> Unit = {}

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window?.setLayout(
                ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.MATCH_PARENT
        )
        window?.setBackgroundDrawable(null)
        setContentView(R.layout.dialog_main_support)
        setCancelable(true)
        setupDialog()
    }

    private fun setupDialog() {
        aboutButton.setOnClickListener {
            onAboutClick(this)
        }
        cancelButton.setOnClickListener {
            dismiss()
        }
        dontShowButton.setOnClickListener {
            onDontShowClick(this)
        }
        twitterDonateImageView.setOnClickListener {
            if (!context.openUrlIntent(context.getString(R.string.twitter_url))) {
                showSnackBar(context.getString(R.string.no_activity_intent))
            }
        }
        coordinatorLayout.setOnClickListener { dismiss() }
    }

    private fun showSnackBar(text: String) {
        Snackbar.make(coordinatorLayout, text, Snackbar.LENGTH_LONG).show()
    }
}