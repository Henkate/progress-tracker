package pl.kuben.progressapp.about

import android.content.Context
import android.graphics.ColorFilter
import android.graphics.PorterDuff
import android.graphics.PorterDuffColorFilter
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.item_contributor.view.*
import pl.kuben.progressapp.R
import pl.kuben.progressapp.data.model.Contributor

class ContributorsAdapter : RecyclerView.Adapter<ContributorsAdapter.ViewHolder>() {

    private var context: Context? = null

    private val inactiveColorFilter: ColorFilter by lazy {
        PorterDuffColorFilter(
                ContextCompat.getColor(context!!, R.color.inactiveIcon),
                PorterDuff.Mode.SRC_IN
        )
    }

    var contributors = listOf<Contributor>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    var onPageClick: (url: String) -> Unit = {}

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_contributor, parent, false)
        return ViewHolder(view)
    }

    override fun getItemCount(): Int = contributors.size

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {
        viewHolder.bind(contributors[position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        init {
            itemView.webImageButton.setOnClickListener {
                contributors[adapterPosition].webUrl.takeIfNotEmpty {
                    onPageClick(this)
                }
            }
            itemView.twitterImageButton.setOnClickListener {
                contributors[adapterPosition].twitterUrl.takeIfNotEmpty {
                    onPageClick(this)
                }
            }
            itemView.githubImageButton.setOnClickListener {
                contributors[adapterPosition].libraryGithubUrl.takeIfNotEmpty {
                    onPageClick(this)
                }
            }
        }

        fun bind(item: Contributor) {
            itemView.twitterImageButton.colorFilter = if (item.twitterUrl.isEmpty()) inactiveColorFilter else null
            itemView.webImageButton.colorFilter = if (item.webUrl.isEmpty()) inactiveColorFilter else null
            itemView.githubImageButton.colorFilter = if (item.libraryGithubUrl.isEmpty()) inactiveColorFilter else null
            itemView.nameTextView.text = item.name
            itemView.descriptionTextView.text = itemView.context.getString(item.descriptionId)
        }
    }

    override fun onAttachedToRecyclerView(recyclerView: RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
        context = recyclerView.context
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        context = null
        super.onDetachedFromRecyclerView(recyclerView)
    }

    //I love extension functions
    private fun String.takeIfNotEmpty(action: String.() -> Unit) {
        if (this.isNotEmpty()) {
            this.action()
        }
    }
}