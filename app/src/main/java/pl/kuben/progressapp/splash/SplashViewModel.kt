package pl.kuben.progressapp.splash

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import io.reactivex.Single
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.common.standardWatch
import pl.kuben.progressapp.data.ResourceProvider
import java.util.concurrent.TimeUnit

private const val SPLASH_TIMER = 1500L
private const val PLAYER_TIMER = 300L

class SplashViewModel(resourceProvider: ResourceProvider) : BaseViewModel(resourceProvider) {

    private val _shouldAnimate = MutableLiveData<Boolean>()
    val shouldAnimate: LiveData<Boolean> by lazy { _shouldAnimate }
    private val _shouldPlay = MutableLiveData<Boolean>()
    val shouldPlay: LiveData<Boolean> by lazy { _shouldPlay }

    fun startAnimationTimer() {
        disposables += Single.timer(SPLASH_TIMER, TimeUnit.MILLISECONDS)
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            _shouldAnimate.value = true
                        }
                )
    }

    fun startPlayerTimer() {
        disposables += Single.timer(PLAYER_TIMER, TimeUnit.MILLISECONDS)
                .standardWatch()
                .subscribeBy(
                        onSuccess = {
                            _shouldPlay.value = true
                        }
                )
    }

}