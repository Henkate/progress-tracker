package pl.kuben.progressapp.plain

import android.arch.lifecycle.Observer
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.fragment_plain.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.progressapp.R
import pl.kuben.progressapp.add.AddActivity
import pl.kuben.progressapp.add.KEY_PROGRESS_ID
import pl.kuben.progressapp.add.KEY_SCREEN
import pl.kuben.progressapp.base.BaseFragment
import pl.kuben.progressapp.details.DetailsActivity
import pl.kuben.progressapp.main.MainActivity
import pl.kuben.progressapp.main.ProgressAdapter

const val NO_ID = -1

class PlainFragment : BaseFragment<PlainViewModel>() {

    override val viewModel: PlainViewModel by viewModel()

    private val adapter = ProgressAdapter()

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_plain, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.progresses.observe(this, Observer {
            if (it != null) {
                adapter.list = it
            }
        })
        setup()
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    private fun setup() {
        adapter.onAddClick = { viewModel.addEntry(it, 1) }
        adapter.onMinusClick = { viewModel.deleteLastEntry(it.id!!) }
        adapter.onDetailsClick = { openDetailsScreen(it) }
        adapter.onEditClick = { openAddScreen(it.id!!) }
        adapter.onDeleteClick = {
            confirmDeletion {
                viewModel.deleteProgress(it.id!!)
            }
        }
        adapter.onItemClick = { openDetailsScreen(it.id!!) }
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(activity)
        viewModel.onCreate()
    }

    private fun confirmDeletion(action: () -> Unit) {
        if (context != null) {
            AlertDialog.Builder(context!!)
                    .setTitle(R.string.confirm_deletion)
                    .setMessage(R.string.confirm_deletion_description)
                    .setPositiveButton(R.string.yes) { dialog, _ ->
                        dialog.dismiss()
                        action()
                    }
                    .setNegativeButton(R.string.no) { dialog, _ ->
                        dialog.dismiss()
                    }
                    .create()
                    .show()
        }
    }

    private fun openDetailsScreen(progressId: Int) {
        val intent = Intent(activity, DetailsActivity::class.java)
        intent.putExtra(pl.kuben.progressapp.details.KEY_PROGRESS_ID, progressId)
        startActivity(intent)
    }

    private fun openAddScreen(progressId: Int = NO_ID) {
        val intent = Intent(activity, AddActivity::class.java)
        intent.putExtra(KEY_PROGRESS_ID, progressId)
        intent.putExtra(KEY_SCREEN, MainActivity.Screen.PLAIN.type)
        startActivity(intent)
    }

}