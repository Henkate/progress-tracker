package pl.kuben.progressapp.view

import android.content.Context
import android.graphics.*
import android.support.annotation.DrawableRes
import android.util.AttributeSet
import android.view.View
import pl.kuben.progressapp.R
import pl.kuben.progressapp.data.model.db.ProgressCount
import pl.kuben.progressapp.drawables.BaseDrawable
import pl.kuben.progressapp.drawables.ColorfulDrawable
import pl.kuben.progressapp.drawables.GraphicDrawable
import pl.kuben.progressapp.drawables.ImageDrawable

const val STYLE_WAVE = 0
const val STYLE_PAINT = 1
const val STYLE_ZIGZAG = 2
const val STYLE_HIVE = 3
const val STYLE_BUBBLES = 4
const val STYLE_RETRO = 5
const val STYLE_WOOD = 6
const val STYLE_GRADIENT = 7

class PrettyProgressBar : View {

    private var fullRect = Rect(0, 0, width, height)
    private var rect = Rect(0, 0, width, height)
    private var bitmapPaint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val blackPaint = Paint(Paint.ANTI_ALIAS_FLAG)
            .apply {
                color = Color.BLACK
                this.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_ATOP)
            }
    private var capsulePath = Path()

    private var canvasBitmap: Bitmap? = null
    private var canvas: Canvas? = null

    var progress = 0
        set(value) {
            field = value
            invalidateRect()
            redraw()
        }
    var max = 1
        set(value) {
            field = value
            invalidateRect()
            redraw()
        }
    var color: Int = Color.RED
        set(value) {
            field = value
            drawable.colorFilter = PorterDuffColorFilter(color, PorterDuff.Mode.SRC_IN)
            redraw()
        }

    var progressBackgroundColor: Int = Color.WHITE
        set(value) {
            field = value
            drawable.progressBackgroundColor = value
            setBackgroundColor(Color.TRANSPARENT)
        }

    var style = 0
        set(value) {
            field = value
            redraw()
        }

    private var drawable: BaseDrawable = GraphicDrawable(resources, style)

    constructor(context: Context?) : super(context)
    constructor(context: Context?, attrs: AttributeSet?) : super(context, attrs) {
        setup(attrs)
    }

    private fun setup(attrs: AttributeSet?) {
        val array = context.obtainStyledAttributes(attrs, R.styleable.PrettyProgressBar)
        progress = array.getInt(R.styleable.PrettyProgressBar_progress, 0)
        max = array.getInt(R.styleable.PrettyProgressBar_max, 1)
        color = array.getColor(R.styleable.PrettyProgressBar_color, Color.RED)
        style = array.getInt(R.styleable.PrettyProgressBar_style, STYLE_WAVE)
        progressBackgroundColor = array.getColor(R.styleable.PrettyProgressBar_backgroundColor, Color.WHITE)
        array.recycle()
    }

    fun setProgress(progress: ProgressCount) {
        with(progress) {
            this@PrettyProgressBar.color = color
            this@PrettyProgressBar.progressBackgroundColor = backgroundColor
            this@PrettyProgressBar.max = maxValue
            this@PrettyProgressBar.progress = count
            this@PrettyProgressBar.style = style
        }

        redraw()
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (canvasBitmap != null) {
            canvas.clipPath(capsulePath)
            canvas.drawBitmap(canvasBitmap!!, 0f, 0f, bitmapPaint)
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        canvasBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888)
        canvas = Canvas(canvasBitmap!!)
        redraw()
    }

    private fun invalidateRect() {
        rect.apply {
            left = paddingLeft
            top = paddingTop
            right = Math.round((width + paddingLeft) * (progress.toFloat() / max.toFloat()))
            bottom = height + paddingTop
        }
        drawable.bounds = rect

        fullRect = Rect(paddingLeft, paddingTop, width - paddingRight, height - paddingBottom)
    }

    private fun redraw() {
        when (style) {
            STYLE_WAVE -> setupGraphicDrawable(R.drawable.wavesmall)
            STYLE_PAINT -> setupGraphicDrawable(R.drawable.paint_path)
            STYLE_ZIGZAG -> setupGraphicDrawable(R.drawable.zigzag)
            STYLE_HIVE -> setupGraphicDrawable(R.drawable.hive)
            STYLE_BUBBLES -> setupGraphicDrawable(R.drawable.bubbles)
            STYLE_GRADIENT -> setupColorfulDrawable(intArrayOf(color, progressBackgroundColor))
            else -> setupGraphicDrawable(R.drawable.bubbles)
        }
        invalidateRect()
        invalidatePath()
        if (canvas != null) {
            drawable.draw(canvas!!)
        }
        invalidate()
    }

    private fun setupColorfulDrawable(colors: IntArray) {
        this.drawable = ColorfulDrawable(resources, style, colors).apply {
            bounds = rect
        }
    }

    private fun setupGraphicDrawable(@DrawableRes drawable: Int) {
        this.drawable = GraphicDrawable(resources, style).apply {
            progressBitmap = BitmapFactory.decodeResource(resources, drawable)
            this.colorFilter = PorterDuffColorFilter(this@PrettyProgressBar.color, PorterDuff.Mode.SRC_IN)
            bounds = rect
            this.progressBackgroundColor = this@PrettyProgressBar.progressBackgroundColor
        }
    }

    private fun setupImageDrawable(@DrawableRes drawable: Int) {
        this.drawable = ImageDrawable(resources, style).apply {
            progressBitmap = BitmapFactory.decodeResource(resources, drawable)
            bounds = rect
        }
    }

    private fun invalidatePath() {
        capsulePath = Path()
        val radius = fullRect.height() / 2f
        val right = fullRect.width() - radius
        with(capsulePath) {
            addRect(radius, 0f, right, fullRect.height().toFloat(), Path.Direction.CW)
            addCircle(radius, radius, radius, Path.Direction.CW)
            addCircle(right, radius, radius, Path.Direction.CW)
        }
    }
}