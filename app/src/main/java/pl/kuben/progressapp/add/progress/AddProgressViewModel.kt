package pl.kuben.progressapp.add.progress

import android.arch.lifecycle.MutableLiveData
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.common.standardWatch
import pl.kuben.progressapp.data.Repository
import pl.kuben.progressapp.data.ResourceProvider
import pl.kuben.progressapp.data.model.db.Progress
import pl.kuben.progressapp.plain.NO_ID
import timber.log.Timber

class AddProgressViewModel(resourceProvider: ResourceProvider, private val repository: Repository)
    : BaseViewModel(resourceProvider) {

    val saved = MutableLiveData<Boolean>()
    val progress = MutableLiveData<Progress>()

    fun addProgress(name: String, maxValue: Int, color: Int, backgroundColor: Int, style: Int) {
        insertProgress(Progress(name, maxValue, color, backgroundColor, style, System.currentTimeMillis()))
    }

    private fun insertProgress(progress: Progress) {
        if (progress.name.isNotEmpty()) {
            progress.id = this.progress.value?.id
            disposables += repository.insertProgress(progress)
                    .standardWatch()
                    .subscribeBy(
                            onSuccess = { saved.value = true },
                            onError = {
                                saved.value = false
                                Timber.e(it)
                            }
                    )
        } else {
            message = getString(R.string.name_not_provided)
        }
    }

    fun getProgress(progressId: Int) {
        if (progressId != NO_ID) {
            disposables += repository.getProgress(progressId)
                    .standardWatch()
                    .subscribeBy(
                            onSuccess = { progress.value = it },
                            onError = { Timber.e(it) }
                    )
        }
    }
}