package pl.kuben.progressapp.webParent

import android.arch.lifecycle.Observer
import android.content.Context
import android.os.Bundle
import android.support.v4.view.ViewPager
import android.support.v7.widget.Toolbar
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import kotlinx.android.synthetic.main.fragment_web_parent.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseFragment

class WebParentFragment : BaseFragment<WebParentViewModel>() {

    override val viewModel: WebParentViewModel by viewModel()

    private lateinit var pagerAdapter: WebViewPagerAdapter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_web_parent, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupScreen()
    }

    private fun setupScreen() {
        setupViewPager()
        subscribeToWebProgresses()
        webProgressToolbar.isVisible = false
        webViewPager.offscreenPageLimit = 1
    }

    override fun onResume() {
        super.onResume()
        viewModel.onResume()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        pagerAdapter = WebViewPagerAdapter(childFragmentManager)
    }

    fun refresh() {
        viewModel.loadWebProgresses()
    }

    private fun subscribeToWebProgresses() {
        viewModel.webProgresses.observe(this, Observer {
            val list = it ?: listOf()
            pagerAdapter.webProgresses = list
            if (list.isNotEmpty()) {
                webFlipper.displayedChild = 0
                webProgressToolbar.isVisible = true
                webProgressToolbar.pageTitle = 0
            } else {
                webProgressToolbar.isVisible = false
                webFlipper.displayedChild = 1
            }
        })
    }

    private fun setupViewPager() {
        webViewPager.adapter = pagerAdapter
        webViewPager.clipToPadding = false
        webViewPager.setPadding(
                resources.getDimensionPixelSize(R.dimen.page_padding_horizontal),
                resources.getDimensionPixelSize(R.dimen.page_padding_vertical),
                resources.getDimensionPixelSize(R.dimen.page_padding_horizontal),
                resources.getDimensionPixelSize(R.dimen.page_padding_vertical)
        )
        webViewPager.addOnPageChangeListener(object : ViewPager.OnPageChangeListener {
            override fun onPageScrollStateChanged(p0: Int) = Unit
            override fun onPageScrolled(p0: Int, p1: Float, p2: Int) = Unit
            override fun onPageSelected(position: Int) {
                webProgressToolbar.pageTitle = position
            }
        })
    }

    private var _pageTitlePosition = 0
    private var Toolbar.pageTitle: Int
        set(value) {
            _pageTitlePosition = value
            val pageTitle = if (value >= pagerAdapter.count) "" else pagerAdapter.getPageTitle(value)
            title = pageTitle
        }
        get() = _pageTitlePosition
}