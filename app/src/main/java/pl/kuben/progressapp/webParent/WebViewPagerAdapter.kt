package pl.kuben.progressapp.webParent

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter
import android.view.View
import android.view.ViewGroup
import pl.kuben.progressapp.data.model.WebProgressEntity
import pl.kuben.progressapp.web.WebFragment

class WebViewPagerAdapter(fragmentManager: FragmentManager) : FragmentStatePagerAdapter(fragmentManager) {

    var webProgresses = listOf<WebProgressEntity>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun getItem(position: Int): Fragment {
        return WebFragment.newInstance(webProgresses[position].id)
    }

    override fun getCount(): Int = webProgresses.size

    override fun getPageTitle(position: Int): CharSequence = webProgresses[position].name

    override fun getItemPosition(`object`: Any): Int {
        return PagerAdapter.POSITION_NONE
    }
}