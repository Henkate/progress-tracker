package pl.kuben.progressapp.details

import android.arch.lifecycle.Observer
import android.graphics.Color
import android.os.Bundle
import android.support.v4.graphics.ColorUtils
import kotlinx.android.synthetic.main.activity_details.*
import org.koin.android.viewmodel.ext.android.viewModel
import pl.kuben.chartview.view.Entry
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseActivity
import pl.kuben.progressapp.common.isVisible
import pl.kuben.progressapp.common.toDateString
import pl.kuben.progressapp.plain.NO_ID
import java.util.*

const val KEY_PROGRESS_ID = "progress_id"

class DetailsActivity : BaseActivity<DetailsViewModel>() {

    override val viewModel: DetailsViewModel by viewModel()

    private var primaryColor: Int = 0
    private var secondaryColor: Int = 0
    private var sortedDates = listOf<Long>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_details)
        setup()
    }

    private fun setup() {
        subscribeToProgress()
        subscribeToEntriesChange()
        setupListeners()
        viewModel.loadData(intent.getIntExtra(KEY_PROGRESS_ID, NO_ID))
    }

    private fun setupListeners() {
        bottomBar.setNavigationOnClickListener {
            onBackPressed()
        }
    }

    private fun subscribeToEntriesChange() {
        viewModel.entries.observe(this, Observer { list ->
            if (list != null && list.isNotEmpty()) {
                val entries = list.map { Entry(it.count, it.date) }
                periodicChartView.entries = entries

                secondPeriodicChartView.tendentious = true
                secondPeriodicChartView.entries = list
                        .map { Entry(it.count, it.date) }
                        .sortedBy { it.date }
                val sorted = entries
                        .asSequence()
                        .map { it.date }
                        .distinct()
                        .sorted()
                        .toList()
                this.sortedDates = sorted
                setupDateRangePicker()
            }
        })
    }

    private fun subscribeToProgress() {
        viewModel.progress.observe(this, Observer {
            primaryColor = it!!.color
            secondaryColor = it.backgroundColor
            setupRangeColors()
        })
    }

    private fun setupDateRangePicker() {
        if (sortedDates.size > 1) {
            dateRangeBar.isVisible = true
            dateRangeBar.tickStart = 1f
            dateRangeBar.tickEnd = sortedDates.size.toFloat()
            dateRangeBar.setOnRangeBarChangeListener { _, leftPinIndex, rightPinIndex, _, _ ->
                val from = sortedDates[leftPinIndex]
                val to = sortedDates[rightPinIndex]
                periodicChartView.fromDate = from
                periodicChartView.toDate = to
                secondPeriodicChartView.fromDate = from
                secondPeriodicChartView.toDate = to
                updateRangeValues(sortedDates, leftPinIndex, rightPinIndex)
            }
            updateRangeValues(sortedDates)
            setupRangeColors()
        } else {
            dateRangeBar.isVisible = false
        }
    }

    private fun updateFormatter() {
        dateRangeBar.setFormatter {
            val id = it.toInt() - 1
            return@setFormatter if (id < sortedDates.size) sortedDates[id].toDateString() else it
        }
    }

    private fun setupRangeColors() {
        periodicChartView.progressColor = primaryColor
        secondPeriodicChartView.progressColor = primaryColor
        dateRangeBar.setTickColor(secondaryColor)
        dateRangeBar.setConnectingLineColor(primaryColor)
        dateRangeBar.setSelectorColor(primaryColor.darkerColor())
        updateFormatter()
    }

    private fun updateRangeValues(sorted: List<Long>, leftPinIndex: Int = 0, rightPinIndex: Int = sorted.size - 1) {
        startRangeTextView.text = sorted[leftPinIndex].toDateString()
        endRangeTextView.text = sorted[rightPinIndex].toDateString()
    }

    private fun Long.getHour(): Int {
        return GregorianCalendar.getInstance()
                .apply { timeInMillis = this@getHour }
                .get(GregorianCalendar.HOUR_OF_DAY)
    }

    private fun Int.darkerColor(ratio: Float = 0.2f): Int {
        return ColorUtils.blendARGB(this, Color.BLACK, ratio)
    }
}