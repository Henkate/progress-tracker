package pl.kuben.progressapp.details

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.plusAssign
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import pl.kuben.progressapp.R
import pl.kuben.progressapp.base.BaseViewModel
import pl.kuben.progressapp.common.standardWatch
import pl.kuben.progressapp.data.Repository
import pl.kuben.progressapp.data.ResourceProvider
import pl.kuben.progressapp.data.model.db.Entry
import pl.kuben.progressapp.data.model.db.Progress
import timber.log.Timber

class DetailsViewModel(
        resourceProvider: ResourceProvider,
        private val repository: Repository
) : BaseViewModel(resourceProvider) {

    private val _entries = MutableLiveData<List<Entry>>()
    val entries: LiveData<List<Entry>> by lazy { _entries }
    private val _progress = MutableLiveData<Progress>()
    val progress: LiveData<Progress> by lazy { _progress }

    fun loadData(progressId: Int) {
        loadProgress(progressId)
    }

    private fun loadEntries(progressId: Int) {
        disposables += repository.getEntries(progressId)
                .standardWatch()
                .subscribeBy(
                        onSuccess = { _entries.value = it },
                        onError = { message = it.message ?: getString(R.string.error) }
                )
    }

    private fun loadProgress(progressId: Int) {
        disposables += repository.getProgress(progressId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy(
                        onSuccess = {
                            _progress.value = it
                            loadEntries(progressId)
                        },
                        onError = { Timber.e(it) }
                )
    }
}