package pl.kuben.progressapp

import android.arch.persistence.room.Room
import android.support.test.InstrumentationRegistry
import android.support.test.runner.AndroidJUnit4
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import pl.kuben.progressapp.data.ProgressDatabase
import pl.kuben.progressapp.data.Repository
import pl.kuben.progressapp.data.dao.*
import pl.kuben.progressapp.data.model.WebProgressEntity
import pl.kuben.progressapp.data.model.db.Entry
import pl.kuben.progressapp.data.model.db.Progress
import pl.kuben.progressapp.data.model.db.WebEntity
import pl.kuben.progressapp.data.model.db.WebProgress
import timber.log.Timber

@RunWith(AndroidJUnit4::class)
class DatabaseTest {

    private lateinit var webEntityDao: WebEntityDao
    private lateinit var webProgressDao: WebProgressDao
    private lateinit var progressCountDao: ProgressCountDao
    private lateinit var progressDao: ProgressDao
    private lateinit var entryDao: EntryDao
    private lateinit var webProgressWithIdsDao: WebProgressWithIdsDao
    private lateinit var database: ProgressDatabase
    private lateinit var repository: Repository

    @Before
    fun setup() {
        val context = InstrumentationRegistry.getTargetContext()
        database = Room.inMemoryDatabaseBuilder(context, ProgressDatabase::class.java).build()
        webEntityDao = database.webEntityDao()
        webProgressDao = database.webProgressDao()
        progressDao = database.progressDao()
        progressCountDao = database.progressCountDao()
        webProgressWithIdsDao = database.webProgressWithIdsDao()
        entryDao = database.entryDao()
        repository = Repository(progressDao, progressCountDao, entryDao, webProgressDao, webProgressWithIdsDao, webEntityDao)
        webProgressWithIdsDao = database.webProgressWithIdsDao()
    }

    @Test
    fun testWebProgress() {
        progressDao.insert(Progress("p1", 10, 0xfff, 0x000, 1, 1))
        progressDao.insert(Progress("p2", 10, 0xfff, 0x000, 1, 1))
        progressDao.insert(Progress("p3", 10, 0xfff, 0x000, 1, 1))
        entryDao.insert(Entry(3, 1, 1))
        entryDao.insert(Entry(5, 1, 2))
        entryDao.insert(Entry(7, 1, 6))
        webProgressDao.addWebProgress(WebProgress("dsd", 0xddd))
        webEntityDao.addWebEntity(WebEntity(1, 1))
        webEntityDao.addWebEntity(WebEntity(2, 1))
        webEntityDao.addWebEntity(WebEntity(3, 1))
        var webProgressWithIds: List<WebProgressEntity>? = null
        repository.getWebProgresses()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeBy {
                    webProgressWithIds = it
                }
        assert(webProgressWithIds != null)
    }

    @After
    fun tearDown() {
        database.close()
    }
}